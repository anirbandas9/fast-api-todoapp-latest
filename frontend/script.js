document.addEventListener("DOMContentLoaded", () => {
  const loginForm = document.getElementById("login-form");
  const todoApp = document.getElementById("todo-app");
  const userDisplay = document.getElementById("user-display");
  const logoutButton = document.getElementById("logout-btn");
  const addTodoForm = document.getElementById("add-todo-form");
  const todoList = document.getElementById("todo-list");

  const BASE_URL = "http://127.0.0.1:8000";

  // Check if the user is already logged in (has a JWT token)
  const token = localStorage.getItem("jwtToken");
  if (token) {
    showTodoApp();
  }

  // Function to handle login form submission
  loginForm.addEventListener("submit", (e) => {
    e.preventDefault();
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    const formData = new URLSearchParams();
    formData.append("grant_type", "password");
    formData.append("username", username);
    formData.append("password", password);

    fetch(`${BASE_URL}/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded", // Set the content type to URL-encoded
        Authorization: "Basic Og==", // If needed, replace this with your actual Authorization header
      },
      body: formData.toString(), // Convert the formData to a URL-encoded string
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.access_token) {
          // Successful authentication, store the JWT token and show the todo app
          localStorage.setItem("jwtToken", data.access_token);
          showTodoApp();
        } else {
          alert("Login failed. Please check your username and password.");
        }
      })
      .catch((error) => {
        console.error("Error:", error);
        alert("An error occurred during login. Please try again later.");
      });
  });

  // Function to show the todo app section and hide the login form
  function showTodoApp() {
    loginForm.style.display = "none";
    todoApp.style.display = "block";
    const token = localStorage.getItem("jwtToken");
    if (token) {
      const decodedToken = parseJwt(token);
      const username = decodedToken.name;
      userDisplay.textContent = username;
      fetchTodoList();
    }
  }

  // Function to fetch and display the user's todo list
  function fetchTodoList() {
    const token = localStorage.getItem("jwtToken");
    if (token) {
      fetch(`${BASE_URL}/api/v1/todo`, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((response) => response.json())
        .then((data) => {
          // Assuming the response data is an array of todo items
          // Update the UI to display the todo list
          const todoListElement = document.getElementById("todo-list");
          todoListElement.innerHTML = "";

          data.forEach((todo) => {
            const listItem = document.createElement("li");
            listItem.textContent = todo.task;

            // Create a delete button (x symbol) for the new todo
            const deleteButton = document.createElement("button");
            deleteButton.textContent = "x";
            deleteButton.setAttribute("data-todo-id", todo.id); // Set the todo ID as a custom attribute
            deleteButton.setAttribute("class", "delete-btn");
            deleteButton.addEventListener("click", () => deleteTodo(todo.id));

            // Create a edit button for each todo
            const editButton = document.createElement("button");
            editButton.textContent = "✎";
            editButton.setAttribute("data-todo-id", todo.id); // Set the todo ID as a custom attribute
            editButton.addEventListener("click", () => editTodoForm(todo.id));

            listItem.appendChild(editButton);
            listItem.appendChild(deleteButton);
            todoListElement.appendChild(listItem);
          });
        })
        .catch((error) => {
          console.error("Error fetching todo list:", error);
          alert(
            "An error occurred while fetching the todo list. Please try again later."
          );
        });
    }
  }

  // Function to handle adding new todos
  addTodoForm.addEventListener("submit", (e) => {
    e.preventDefault();
    const todoTitle = document.getElementById("todo-title").value;

    const token = localStorage.getItem("jwtToken");
    if (token) {
      const newTodo = { task: todoTitle }; // Create the JSON object with the task data

      fetch(`${BASE_URL}/api/v1/todo`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(newTodo), // Convert the newTodo object to JSON string
      })
        .then((response) => response.json())
        .catch((error) => {
          console.error("Error adding todo:", error);
          alert(
            "An error occurred while adding the todo. Please try again later."
          );
        });
    }
  });

  // Function to handle deleting a todo
  function deleteTodo(todoId) {
    const token = localStorage.getItem("jwtToken");
    if (token) {
      fetch(`${BASE_URL}/api/v1/todo/${todoId}`, {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((response) => {
          if (response.status === 200) {
            // Successfully deleted, remove the todo from the UI
            const todoItem = document.querySelector(
              `li button[data-todo-id="${todoId}"]`
            );
            if (todoItem) {
              todoItem.parentElement.remove();
            }
          } else {
            // Handle error response (if needed)
            console.error("Error deleting todo:", response);
          }
        })
        .catch((error) => {
          console.error("Error deleting todo:", error);
          alert(
            "An error occurred while deleting the todo. Please try again later."
          );
        });
    }
  }

  // Function to parse the JWT token and extract data from it
  function parseJwt(token) {
    try {
      return JSON.parse(atob(token.split(".")[1]));
    } catch (e) {
      return null;
    }
  }

  // Logout functionality
  logoutButton.addEventListener("click", () => {
    localStorage.removeItem("jwtToken");
    loginForm.style.display = "block";
    todoApp.style.display = "none";
  });

  function updateTodo(todoId, updatedTitle) {
    const token = localStorage.getItem("jwtToken");
    if (token) {
      const updatedTodo = { task: updatedTitle }; // Create the JSON object with the updated task data

      fetch(`${BASE_URL}/api/v1/todo/${todoId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(updatedTodo), // Convert the updatedTodo object to JSON string
      })
        .then((response) => response.json())
        .then((data) => {
          location.reload();
          // Assuming the response data is the updated todo object
          // Update the UI to display the updated todo
          const listItem = document.querySelector(
            `li button[data-todo-id="${todoId}"]`
          ).parentElement;
          listItem.textContent = data.task;

          // Show the todo item again after updating
          listItem.style.display = "block";
        })
        .catch((error) => {
          console.error("Error updating todo:", error);
          alert(
            "An error occurred while updating the todo. Please try again later."
          );
        });
    }
  }

  // Function to handle editing a todo
  function editTodoForm(todoId) {
    const listItem = document.querySelector(
      `li button[data-todo-id="${todoId}"]`
    ).parentElement;
    const todoTitle = listItem.textContent;
    // Hide the todo item while editing
    listItem.style.display = "none";

    // Create the edit form
    const editForm = document.createElement("form");
    const editInput = document.createElement("input");
    editInput.type = "text";
    editInput.value = todoTitle;
    const updateButton = document.createElement("button");
    updateButton.textContent = "Update";
    const cancelButton = document.createElement("button");
    cancelButton.textContent = "Cancel";

    editForm.appendChild(editInput);
    editForm.appendChild(updateButton);
    editForm.appendChild(cancelButton);

    // Handle form submission
    editForm.addEventListener("submit", (e) => {
      e.preventDefault();
      const updatedTitle = editInput.value.trim();

      if (updatedTitle) {
        updateTodo(todoId, updatedTitle);
        listItem.style.display = "block"; // Show the todo item again
        editForm.remove(); // Remove the edit form after submission
      }
    });

    // Handle cancel button click
    cancelButton.addEventListener("click", () => {
      listItem.style.display = "block"; // Show the todo item again without changes
      editForm.remove(); // Remove the edit form without making any changes
      //   reload page to show changes
      location.reload();
    });

    // Add the edit form before the todo item
    listItem.parentNode.insertBefore(editForm, listItem);
  }
});