document.addEventListener("DOMContentLoaded", () => {
  const registrationForm = document.getElementById("registration-form");
  const BASE_URL = "http://127.0.0.1:8000";

  // Function to handle registration form submission
  registrationForm.addEventListener("submit", (e) => {
    e.preventDefault();

    const name = document.getElementById("name").value;
    const username = document.getElementById("email").value;
    const password = document.getElementById("password").value;

    const jsonData = {
      name: name,
      email: username,
      password: password,
    };

    fetch(`${BASE_URL}/api/v1/user/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(jsonData),
    })
      .then((response) => response.json())
      .then((data) => {
        data.name ? location.href = "index.html" : alert("Registration failed. Please try again later.");
      })
      .catch((error) => {
        console.error("Error:", error);
        alert("An error occurred during registration. Please try again later.");
      });
  });
});