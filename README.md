# FASTAPI TODO APP

## Guide

The app is made using JWT authentication and FastAPI framework. The frontend is made using HTML, CSS and Javascript.
Once authenticated the user can add, edit or delete his todo list. The user can view all the todo list of all the users.
The API ENDPOINTS are RESTful and are made using FastAPI framework.

## Installation

Goto todoapp folder
    
```bash
cd todoapp
```

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the requirements.

```bash
pip install -r requirements.txt
```

## Usage

Run the fastapi app using uvicorn

```bash
uvicorn main:app --reload
```

Open the index.html file in the browser

```bash
index.html
```

## API ENDPOINTS

### Authentication

### 1. /login - POST

- This endpoint is used to login the user and generate the JWT token.

### 2. /api/v1/user/ - POST

- This endpoint is used to create a new user.

<br>

### USERS ENDPOINTS

### 3. /api/v1/user/{user_id} - GET

- This endpoint is used to get the user details.

### 4. /api/v1/user/todos/{id} - GET

- This endpoint is used to get the todo list of the user.

### 5. /api/v1/user/{id} - DELETE

- This endpoint is used to delete the user.

<br>

### TODOS ENDPOINTS

### 6. /api/v1/todo/ - GET

- This endpoint is used to get all the todos of all the users.

### 7. /api/v1/todo/ - POST

- This endpoint is used to create a new todo for the authenticated user.

### 8. /api/v1/todo/{id} - GET

- This endpoint is used to get the todo details.

### 9. /api/v1/todo/{id} - PUT

- This endpoint is used to update the todo details only for the creator of the todo.

### 10. /api/v1/todo/{id} - DELETE

- This endpoint is used to delete the todo only for the creator of the todo.

