from fastapi import APIRouter, Depends, status, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from app import database, models, token
from sqlalchemy.orm import Session
from app.hashing import Hash

router = APIRouter(tags=["Authentication"])
get_db = database.get_db


@router.post("/login")
async def login(request: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    user = db.query(models.User).filter(
        models.User.email == request.username).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Invalid Credentials")
    if not Hash.verify(user.password, request.password):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Incorrect Password")
    # generate a jwt token and return
    access_token = token.create_access_token(data={"sub": user.email, "name": user.name})
    return {"access_token": access_token, "token_type": "bearer"}
