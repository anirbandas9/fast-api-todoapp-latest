from fastapi import APIRouter, Depends, status
from sqlalchemy.orm import Session
from app import schemas, database
from app.oauth2 import get_current_user
from app.repository import user

router = APIRouter(
    prefix="/api/v1/user",
    tags=['Users'],
)
get_db = database.get_db


@router.post("/", status_code=status.HTTP_201_CREATED, response_model=schemas.ShowUser)
def create_user(request: schemas.User, db: Session = Depends(get_db)):
    return user.create_user(request, db)


@router.get("/{id}", response_model=schemas.ShowUser)
def get_user(id: int, db: Session = Depends(get_db)):
    return user.get_user(id, db)


@router.delete("/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_user(id: int, db: Session = Depends(get_db), current_user: schemas.User = Depends(get_current_user)):
    return user.delete_user(id, db)


@router.get("/{id}/todos", response_model=schemas.ShowUserWithTodos)
def get_user_with_todos(id: int, db: Session = Depends(get_db), current_user: schemas.User = Depends(get_current_user)):
    return user.get_user_with_todos(id, db)


# get single todo of user
@router.get("/{user_id}/todos/{todo_id}")
def get_todo_of_user(user_id: int, todo_id: int, db: Session = Depends(get_db),
                     current_user: schemas.User = Depends(get_current_user)):
    return user.get_todo_of_user(user_id, todo_id, db)
