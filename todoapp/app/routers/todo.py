from typing import List
from fastapi import APIRouter, Depends, status
from sqlalchemy.orm import Session
from app import schemas, database, models
from app.oauth2 import get_current_user
from app.repository import todo

router = APIRouter(
    prefix="/api/v1/todo",
    tags=['Todos'],
)
get_db = database.get_db


@router.get("/", response_model=List[schemas.TodoWithId])
async def all(db: Session = Depends(get_db), current_user: schemas.User = Depends(get_current_user)):
    return db.query(models.Todo).all()


@router.get("/{id}", response_model=schemas.ShowTodo)
async def get(id: int, db: Session = Depends(get_db), current_user: schemas.User = Depends(get_current_user)):
    return todo.get_todo(id, db)


@router.post("/", status_code=status.HTTP_201_CREATED, response_model=schemas.ShowTodo)
async def create(request: schemas.Todo, db: Session = Depends(get_db),
                 current_user: schemas.User = Depends(get_current_user)):
    return todo.create_todo(request, db, current_user)


@router.put("/{id}", status_code=status.HTTP_202_ACCEPTED, response_model=schemas.ShowTodo)
async def update(id: int, request: schemas.Todo, db: Session = Depends(get_db),
                 current_user: schemas.User = Depends(get_current_user)):
    return todo.update_todo(id, request, db, current_user)


# only user who created the todo can delete it
@router.delete("/{id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete(id: int, db: Session = Depends(get_db), current_user: schemas.User = Depends(get_current_user)):
    return todo.delete_todo(id, db, current_user)
