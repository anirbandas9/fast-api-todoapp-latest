from pydantic import BaseModel
from typing import List, Optional


class User(BaseModel):
    name: str
    email: str
    password: str


class ShowUser(BaseModel):
    name: str
    email: str

    class Config():
        orm_mode = True


class Todo(BaseModel):
    task: str


class TodoWithId(Todo):
    id: int

    class Config():
        orm_mode = True


class ShowTodo(BaseModel):
    task: str
    creator: ShowUser

    class Config():
        orm_mode = True


class ShowUserWithTodos(BaseModel):
    name: str
    email: str
    todos: List[Todo] = []

    class Config():
        orm_mode = True


class Login(BaseModel):
    username: str
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Optional[str] = None
