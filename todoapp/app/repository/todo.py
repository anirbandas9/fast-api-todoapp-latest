from fastapi import HTTPException, status
from sqlalchemy.orm import Session
from app import models, schemas


def get_todo(id: int, db: Session):
    todo = db.query(models.Todo).filter(models.Todo.id == id).first()
    if not todo:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Todo with id {id} not found")
    return todo


def create_todo(request: schemas.Todo, db: Session, current_user):
    new_todo = models.Todo(task=request.task, user_id=current_user.id)
    db.add(new_todo)
    db.commit()
    db.refresh(new_todo)
    return new_todo


def update_todo(id: int, request: schemas.Todo, db: Session, current_user):
    todo = db.query(models.Todo).filter(models.Todo.id == id).first()
    if not todo:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Todo with id {id} not found")
    if todo.user_id != current_user.id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f"User with id {current_user.id} not allowed to update this todo")
    todo.task = request.task
    db.commit()
    db.refresh(todo)
    return todo


def delete_todo(id: int, db: Session, current_user):
    todo = db.query(models.Todo).filter(models.Todo.id == id)
    if not todo.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Todo with id {id} not found")
    if todo.first().user_id != current_user.id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f"User with id {current_user.id} not allowed to delete this todo")
    todo.delete(synchronize_session=False)
    db.commit()
    return "done"
