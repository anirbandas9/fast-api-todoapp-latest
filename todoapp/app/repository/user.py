from fastapi import HTTPException, status
from sqlalchemy.orm import Session
from app import models, schemas
from app.hashing import Hash


def create_user(request: schemas.User, db: Session):
    if db.query(models.User).filter(models.User.email == request.email).first():
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f"User with email {request.email} already exists")
    hashed_password = Hash.bcrypt(request.password)
    new_user = models.User(name=request.name, email=request.email,
                           password=hashed_password)
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user


def get_user(id: int, db: Session):
    user = db.query(models.User).filter(models.User.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User with id {id} not found")
    return user


def delete_user(id: int, db: Session):
    user = db.query(models.User).filter(models.User.id == id)
    if not user.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User with id {id} not found")
    # also delete there respective todos
    todos = db.query(models.Todo).filter(models.Todo.user_id == id).all()
    for todo in todos:
        db.delete(todo)
    # delete the user
    user.delete(synchronize_session=False)
    db.commit()
    return "done"


def get_user_with_todos(id: int, db: Session):
    user = db.query(models.User).filter(models.User.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User with id {id} not found")
    return user


def get_todo_of_user(user_id: int, todo_id: int, db: Session):
    todo = db.query(models.Todo).filter(models.Todo.id == todo_id, models.Todo.user_id == user_id).first()
    if not todo:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User with id {user_id} or todo with id {todo_id} not found")
    return {"task": todo.task}
