from fastapi import FastAPI
from app import models
from app.database import engine
from app.routers import user, todo, authentication
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

# CORS
origins = [
    "http://127.0.0.1:5500",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "PUT", "POST", "DELETE"],
    allow_headers=["*"],
)

models.Base.metadata.create_all(engine)

app.include_router(authentication.router)
app.include_router(user.router)
app.include_router(todo.router)


@app.get("/", tags=["Root"])
async def root():
    return {"message": "Welcome to the Todo App"}
